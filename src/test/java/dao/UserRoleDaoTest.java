package dao;

import context.TestConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.artezio.dao.UserRoleDao;
import ru.artezio.model.UserRole;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Transactional
public class UserRoleDaoTest extends TestConfiguration {

    @PersistenceContext private EntityManager entityManager;
    @Autowired private UserRoleDao userRoleDao;
    private static final String LIST_ROLES = "select * from roles";

    @Test
    public void testGetAll() throws Exception {
        List<UserRole> userRoles = userRoleDao.list();
        Query query = entityManager.createNativeQuery(LIST_ROLES, UserRole.class);
        List<UserRole> list = query.getResultList();

        assertNotNull(userRoles);
        assertTrue(list.equals(userRoles));
    }
}

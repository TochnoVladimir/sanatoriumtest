package controller;

import org.codehaus.jackson.map.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import ru.artezio.controller.UserController;
import ru.artezio.model.UserRole;
import ru.artezio.model.dto.UserDto;
import ru.artezio.model.enumerated.UserStatus;
import ru.artezio.service.UserService;

import javax.annotation.Resource;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
        @ContextConfiguration(loader = AnnotationConfigWebContextLoader.class),
        @ContextConfiguration(locations = "file:src/test/resources/app-context-test.xml")

})
public class UserControllerTest {

    @Configuration
    static class ContextConfigurationConfig {
        @Bean
        public UserService userService() {
            UserService userService = mock(UserService.class);
            return userService;
        }

        @Bean
        public UserController userController() {
            UserController userController = new UserController();
            return userController;
        }

        @Bean
        public Mapper mapper() {
            List<String> list = new ArrayList<String>();
            list.add("dozer-mapping.xml");
            return new DozerBeanMapper();
        }
    }

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Resource private WebApplicationContext wac;
    @Autowired private UserController userController;
    @Autowired private UserService userService;
    private MockMvc mockMvc;
    private Long userId = 1L;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void simple() throws Exception {
        mockMvc.perform(get("/users/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype(),
                        Charset.forName("utf8"))))
                .andReturn();
    }

    @Test
    public void testGetList() throws Exception {
        Integer count = 3;
        List<UserDto> list = createListUser(count);
        when(userService.list()).thenReturn(list);
        for (int i = 0; i < count; i++) {
            mockMvc.perform(get("/users/"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.success", is(true)))
                    .andExpect(jsonPath("$.result[" + i + "].id", is(list.get(i).getId().intValue())))
                    .andExpect(jsonPath("$.result[" + i + "].login", is(list.get(i).getLogin())))
                    .andExpect(jsonPath("$.result[" + i + "].password", is(list.get(i).getPassword())))
                    .andExpect(jsonPath("$.result[" + i + "].email", is(list.get(i).getEmail())))
                    .andExpect(jsonPath("$.result[" + i + "].status", is(list.get(i).getStatus().toString())))
                    .andExpect(jsonPath("$.result[" + i + "].roles[0].name", is(((UserRole) list.get(i).getRoles().toArray()[0]).getName())))
                    .andExpect(jsonPath("$.result[" + i + "].roles[1].name", is(((UserRole) list.get(i).getRoles().toArray()[1]).getName())))
                    .andReturn();
        }
    }

    @Test
    public void testCreateUser() throws Exception {
        UserDto user = createUser(userId);
        when(userService.create((UserDto) anyObject())).thenReturn(user);
        mockMvc.perform(post("/users/").content(new ObjectMapper().writeValueAsBytes(user))
                .contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype(),
                        Charset.forName("utf8")
                )))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.result.id", is(user.getId().intValue())))
                .andExpect(jsonPath("$.result.login", is(user.getLogin())))
                .andExpect(jsonPath("$.result.password", is(user.getPassword())))
                .andExpect(jsonPath("$.result.email", is(user.getEmail())))
                .andExpect(jsonPath("$.result.status", is(user.getStatus().toString())))
                .andExpect(jsonPath("$.result.roles[0].name", is(((UserRole) user.getRoles().toArray()[0]).getName())))
                .andExpect(jsonPath("$.result.roles[1].name", is(((UserRole) user.getRoles().toArray()[1]).getName())));
    }

    @Test
    public void testFailedCreateUser() throws Exception {
        UserDto user = new UserDto();
        user.setEmail("email");
        when(userService.create((UserDto) anyObject())).thenReturn(user);
        mockMvc.perform(post("/users/").content(new ObjectMapper().writeValueAsBytes(user))
                .contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype(),
                        Charset.forName("utf8")
                )))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", is(false)))
                .andExpect(jsonPath("$.errorMessage", is("{password=Пароль не указан., " +
                        "login=Логин не указан., email=Неверный email.}")));
    }

    @Test
    public void testFindUserById() throws Exception {
        UserDto user = createUser(userId);
        when(userService.findById(anyLong())).thenReturn(user);
        mockMvc.perform(get("/users/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.result.id", is(user.getId().intValue())))
                .andExpect(jsonPath("$.result.login", is(user.getLogin())))
                .andExpect(jsonPath("$.result.password", is(user.getPassword())))
                .andExpect(jsonPath("$.result.email", is(user.getEmail())))
                .andExpect(jsonPath("$.result.status", is(user.getStatus().toString())))
                .andExpect(jsonPath("$.result.roles[0].name", is(((UserRole) user.getRoles().toArray()[0]).getName())))
                .andExpect(jsonPath("$.result.roles[1].name", is(((UserRole) user.getRoles().toArray()[1]).getName())));
    }

    @Test
    public void testDeleteUser() throws Exception {
        UserDto user = createUser(userId);
        mockMvc.perform(delete("/users/1").content(new ObjectMapper().writeValueAsBytes(user))
                .contentType(new MediaType(MediaType.APPLICATION_JSON.getType(),
                        MediaType.APPLICATION_JSON.getSubtype(),
                        Charset.forName("utf8")
                )))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", is(true)));
    }

    private UserDto createUser(Long id) {
        UserDto user = new UserDto();
        user.setLogin("testLogin");
        user.setPassword("testPassword");
        user.setEmail("alex111@devcolibri.com");
        user.setStatus(UserStatus.ACTIVE);
        user.setRoles(createSetUserRoles());
        if (!(id == null)) {
            user.setId(id);
        }
        return user;
    }

    private List<UserDto> createListUser(Integer count) {
        List<UserDto> list = new ArrayList<UserDto>();
        UserDto user;
        for (int i = 1; i <= count; i++) {
            user = new UserDto();
            user.setLogin("testLogin-" + i);
            user.setPassword("testPassword-" + i);
            user.setEmail("alex+27@gmail.com");
            user.setStatus(UserStatus.ACTIVE);
            user.setRoles(createSetUserRoles());
            user.setId(new Long(i));
            list.add(user);
        }
        return list;
    }

    private Set<UserRole> createSetUserRoles() {
        Set<UserRole> set = new HashSet<UserRole>();
        UserRole userRole = new UserRole("Admin");
        userRole.setId(1L);
        set.add(userRole);
        userRole = new UserRole("User");
        userRole.setId(2L);
        set.add(userRole);
        return set;
    }
}
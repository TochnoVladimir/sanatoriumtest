import { Component, Output, Input, EventEmitter, OnChanges, OnInit  } from '@angular/core';

import {Rooms} from "../../shared/model/rooms";
import {Housing} from "../../shared/model/housing";
import {Category} from "../../shared/model/category";
import {Condition} from "../../shared/model/condition";
import {HousingService} from "../../shared/service/housing.service";
import {ConditionService} from "../../shared/service/condition.service";
import {CategoryService} from "../../shared/service/category.service";

@Component({
    moduleId: module.id,
    selector: 'rooms-form',
    templateUrl: 'rooms-form.component.html',
    styleUrls: ['rooms-form.component.css']
})
export class RoomsFormComponent implements OnInit, OnChanges {
    name:string = '';
    categoryRoom:any;
    conditionRoom:Condition;
    housing:Housing;

    @Input() room:any;
    @Output() create:EventEmitter<Rooms> = new EventEmitter();

    conditions:Condition[];
    categorys:Category[];
    housings:Housing[];

    constructor(private conditionService:ConditionService, private housingService:HousingService,
                private categoryService:CategoryService) {
        this.conditions = [];
        this.categorys = [];
        this.housings = [];
    }

    ngOnInit() {
        this.conditionService.getCondition().subscribe(conditions => this.conditions = conditions);
        this.categoryService.getCategory().subscribe(categorys => this.categorys = categorys);
        this.housingService.getHousing().subscribe(housings => this.housings = housings);
    }

    ngOnChanges() {
        if ((!!this.room)) {
            this.name = this.room.roomIn.name;
            this.categoryRoom = this.categorys[this.findIndex(this.categorys, this.room.roomIn.categoryRoom)];
            this.conditionRoom = this.conditions[this.findIndex(this.conditions, this.room.roomIn.conditionRoom)];
            this.housing = this.housings[this.findIndex(this.housings, this.room.roomIn.housing)];
        }
    }

    findIndex(items:any[], item:any):number {
        for (var i = 0; i < items.length; i++) {
            if (items[i].id == item.id) {
                return i;
            }
        }
    }

    onSubmit() {
        if (this.room) {
            let roomsUp:Rooms = new Rooms(this.name, this.categoryRoom, this.conditionRoom, this.housing);
            roomsUp.id = this.room.roomIn.id;
            this.create.emit(roomsUp);
        }
        if (!this.room) {
            let rooms:Rooms = new Rooms(this.name, this.categoryRoom, this.conditionRoom, this.housing);
            this.create.emit(rooms);
        }
        this.onReset();
    }

    onReset() {
        this.room = undefined;
        this.categoryRoom = null;
        this.name = '';
        this.conditionRoom = null;
        this.housing = null;
    }
}
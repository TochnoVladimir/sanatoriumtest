import {Component, Input, Output, EventEmitter } from '@angular/core';
import {Rooms} from "../../shared/model/rooms";

@Component({
    moduleId: module.id,
    selector: 'rooms-item',
    templateUrl: 'rooms-item.component.html',
    styleUrls: ['rooms-item.component.css']
})

export class RoomsItemComponent {
    @Input() room:Rooms;
    @Output() delete = new EventEmitter();
    @Output() update = new EventEmitter();

    onUpdate() {
        this.update.emit(this.room);
    }

    onDelete() {
        if(confirm("Вы хотите удалить запись?")){
            this.delete.emit(this.room);
        }
    }
}
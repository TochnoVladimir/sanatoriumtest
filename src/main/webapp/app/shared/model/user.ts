export class User {
    id:number;

    constructor(public login:string,
                public password:string,
                public email:string,
                public status:string = 'ACTIVE',
                public roles:[{id:number, name:string}] = [{id: 3, name: 'Guest'}]) {
    }
}
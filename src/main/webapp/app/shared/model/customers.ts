export class Customers {
    id:number;

    constructor(public name:string,
                public surname:string,
                public patronymic:string,
                public passportSeries:string,
                public passportId:number,
                public email:string,
                public phone:string) {
    }
}
import { Http,Headers,RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {Condition} from "../model/condition";

@Injectable()
export class ConditionService {

    private apiUrl = '/artezio/conditions';

    constructor(private http:Http) {
    }

    getCondition() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get(this.apiUrl)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError);

    }

    createCondition(condition:Condition) {
        if (!!!condition.id) {
            return this.addCondition(condition);
        }
        if (condition.id) {
            return this.updateCondition(condition);
        }
    }

    addCondition(condition:Condition) {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post(this.apiUrl, condition, this.createOption)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError)
    }

    updateCondition(condition:Condition) {
        let url = `${this.apiUrl}/${condition.id}`;
        //noinspection TypeScriptUnresolvedFunction
        return this.http.put(url, condition, this.createOption)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError)
    }

    deleteCondition(condition:Condition) {
        let url = `${this.apiUrl}/${condition.id}`;

        //noinspection TypeScriptUnresolvedFunction
        return this.http.delete(url, this.createOption)
            .map(res => {
                    if (!res.json().success) {
                        alert(res.json().errorMessage);
                        return false
                    } else {
                        return true;
                    }
                }
            )
            .catch(this.handleError);
    }

    createOption() {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        return options;
    }

    handlingResponse(res:any) {
        if (!res.json().success) {
            alert(res.json().errorMessage);
        } else {
            return res.json().result as Condition[];
        }
    }

    private handleError(error:any) {
        console.error('Произошла ошибка');
        return Observable.throw(error.message || error);
    }
}
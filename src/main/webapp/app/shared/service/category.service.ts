﻿import { Http,Headers,RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {Category} from "../model/category";

@Injectable()
export class CategoryService {

    private apiUrl = '/artezio/categorys';

    constructor(private http:Http) {
    }

    getCategory() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get(this.apiUrl)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError);
    }

    createCategory(category:Category) {
        if (!!!category.id) {
            return this.addCategory(category);
        }
        if (category.id) {
            return this.updateCategory(category);
        }
    }

    addCategory(category:Category) {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post(this.apiUrl, category, this.createOption)
            .map(res => this.handlingResponse(res))
            .catch(this.handleError)
    }

    updateCategory(category:Category) {
        let url = `${this.apiUrl}/${category.id}`;
        //noinspection TypeScriptUnresolvedFunction
        return this.http.put(url, category, this.createOption)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError)
    }

    deleteCategory(category:Category) {
        let url = `${this.apiUrl}/${category.id}`;

        //noinspection TypeScriptUnresolvedFunction
        return this.http.delete(url, this.createOption)
            .map(res => {
                    if (!res.json().success) {
                        alert(res.json().errorMessage);
                        return false
                    } else {
                        return true;
                    }
                }
            )
            .catch(this.handleError);
    }

    createOption() {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        return options;
    }

    handlingResponse(res:any) {
        if (!res.json().success) {
            alert(res.json().errorMessage);
        } else {
            return res.json().result as Category[];
        }
    }

    private handleError(error:any) {
        console.error('Произошла ошибка');
        return Observable.throw(error.message || error);
    }
}
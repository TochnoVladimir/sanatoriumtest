import { Http,Headers,RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {Housing} from "../model/housing";

@Injectable()
export class HousingService {

    private apiUrl = '/artezio/housings';

    constructor(private http:Http) {
    }

    getHousing() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get(this.apiUrl)
            .map(res => this.handlingResponse(res))
            .catch(this.handleError);

    }

    createHousing(housing:Housing) {
        if (!!!housing.id) {
            return this.addHousing(housing);
        }
        if (housing.id) {
            return this.updateHousing(housing);
        }
    }

    addHousing(housing:Housing) {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post(this.apiUrl, housing, this.createOption)
            .map(res => this.handlingResponse(res))
            .catch(this.handleError)
    }

    updateHousing(housing:Housing) {
        let url = `${this.apiUrl}/${housing.id}`;
        //noinspection TypeScriptUnresolvedFunction
        return this.http.put(url, housing, this.createOption)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError)
    }

    deleteHousing(housing:Housing) {
        let url = `${this.apiUrl}/${housing.id}`;

        //noinspection TypeScriptUnresolvedFunction
        return this.http.delete(url, this.createOption)
            .map(res => {
                    if (!res.json().success) {
                        alert(res.json().errorMessage);
                        return false
                    } else {
                        return true;
                    }
                }
            )
            .catch(this.handleError);
    }

    createOption() {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        return options;
    }

    handlingResponse(res:any) {
        if (!res.json().success) {
            alert(res.json().errorMessage);
        } else {
            return res.json().result as Housing[];
        }
    }

    private handleError(error:any) {
        console.error('Произошла ошибка');
        return Observable.throw(error.message || error);
    }
}
import { Component, Output, Input, EventEmitter, OnChanges } from '@angular/core';
import {User} from "../../shared/model/user";

@Component({
    moduleId: module.id,
    selector: 'user-form',
    templateUrl: 'user-form.component.html',
    styleUrls: ['user-form.component.css']
})
export class UserFormComponent implements OnChanges {

    login:string = '';
    password:string = '';
    email:string = '';
    status:string = '';
    roles:{id:number, name:string}[] = [];

    @Input() user:any;
    @Output() create:EventEmitter<User> = new EventEmitter();

    ngOnChanges() {
        if ((!!this.user)) {
            this.login = this.user.userIn.login;
            this.email = this.user.userIn.email;
            this.status = this.user.userIn.status;
        }
    }

    onSubmit() {
        if (this.user) {
            let userUp:User = new User(this.login, this.password, this.email, this.user.userIn.status, this.user.userIn.roles);
            userUp.id = this.user.userIn.id;
            this.create.emit(userUp);
        }
        if (!this.user) {
            let user:User = new User(this.login, this.password, this.email);
            this.create.emit(user);
        }
        this.onReset();
    }

    onReset() {
        this.user = undefined;
        this.login = '';
        this.password = '';
        this.email = '';
    }
}
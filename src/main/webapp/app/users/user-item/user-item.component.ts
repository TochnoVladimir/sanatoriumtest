import {Component, Input, Output, EventEmitter } from '@angular/core';
import {User} from "../../shared/model/user";

@Component({
    moduleId: module.id,
    selector: 'user-item',
    templateUrl: 'user-item.component.html',
    styleUrls: ['user-item.component.css']
})

export class UserItemComponent {
    @Input() user:User;
    @Output() delete = new EventEmitter();
    @Output() update = new EventEmitter();

    onUpdate() {
        this.update.emit(this.user);
    }

    onDelete() {
        if(confirm("Вы хотите удалить запись?")){
            this.delete.emit(this.user);
        }
    }
}
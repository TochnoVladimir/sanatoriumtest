import {Component, OnInit} from '@angular/core';
import {User} from "../shared/model/user";
import {UserService} from "../shared/service/user.service";

@Component({
    moduleId: module.id,
    selector: 'users',
    templateUrl: 'users.component.html',
    styleUrls: ['users.component.css']
})

export class UsersComponent implements OnInit {
    users:User[];
    user:{userIn:User, flaq:number};

    constructor(private userService:UserService) {
        this.users = [];
    }

    ngOnInit() {
        this.userService.getUsers().subscribe(users => this.users = users);
    }

    create(user:User) {
        this.userService.createUser(user).subscribe(
            userResponse=> {
                if (!!userResponse) {
                    if (!!user.id) {
                        let index = this.findUserIndex(userResponse);
                        this.users[index] = userResponse;
                    } else {
                        this.users.push(userResponse);
                    }

                }
            });
    }

    findUserIndex(item:User):number {
        for (var i = 0; i < this.users.length; i++) {
            if (this.users[i].id == item.id) {
                return i;
            }
        }
    }

    update(user:User) {
        this.user = {
            userIn: user,
            flaq: Math.random()
        }
    }

    delete(user:User) {
        this.userService.deleteUser(user).subscribe(
            res => {
                if (res) {
                    let index = this.users.indexOf(user);
                    if (index > -1) {
                        this.users.splice(index, 1);
                    }
                }
            });
    }
}
import {Component, OnInit} from '@angular/core';

import {Condition} from "../shared/model/condition";
import {ConditionService} from "../shared/service/condition.service";


@Component({
    moduleId: module.id,
    selector: 'condition',
    templateUrl: 'condition.component.html',
    styleUrls: ['condition.component.css']
})

export class ConditionComponent implements OnInit {
    conditions:Condition[];
    condition:{conditionIn:Condition, flaq:number};

    constructor(private conditionService:ConditionService) {
        this.conditions = [];
    }

    ngOnInit() {
        this.conditionService.getCondition().subscribe(conditions => this.conditions = conditions);
    }

    create(condition:Condition) {
        this.conditionService.createCondition(condition).subscribe(
            conditionResponse=> {
                if (!!conditionResponse) {
                    if (!!condition.id) {
                        let index = this.findIndex(conditionResponse);
                        this.conditions[index] = conditionResponse;
                    } else {
                        this.conditions.push(conditionResponse);
                    }

                }
            });
    }

    findIndex(item:Condition):number {
        for (var i = 0; i < this.conditions.length; i++) {
            if (this.conditions[i].id == item.id) {
                return i;
            }
        }
    }

    update(condition:Condition) {
        this.condition = {
            conditionIn: condition,
            flaq: Math.random()
        }
    }

    delete(condition:Condition) {
        this.conditionService.deleteCondition(condition).subscribe(
            res => {
                if (res) {
                    let index = this.conditions.indexOf(condition);
                    if (index > -1) {
                        this.conditions.splice(index, 1);
                    }
                }
            });
    }
}
import { Component, Output, Input, EventEmitter, OnChanges } from '@angular/core';
import {Housing} from "../../shared/model/housing";

@Component({
    moduleId: module.id,
    selector: 'housing-form',
    templateUrl: 'housing-form.component.html',
    styleUrls: ['housing-form.component.css']
})
export class HousingFormComponent implements OnChanges {
    name:string = '';

    @Input() housing:any;
    @Output() create:EventEmitter<Housing> = new EventEmitter();

    ngOnChanges() {
        if ((!!this.housing)) {
            this.name = this.housing.housingIn.name;
        }
    }

    onSubmit() {
        if (this.housing) {
            let housingUp:Housing = new Housing(this.name);
            housingUp.id = this.housing.housingIn.id;
            this.create.emit(housingUp);
        }
        if (!this.housing) {
            let housing:Housing = new Housing(this.name);
            this.create.emit(housing);
        }
        this.onReset();
    }

    onReset() {
        this.housing = undefined;
        this.name = '';
    }
}
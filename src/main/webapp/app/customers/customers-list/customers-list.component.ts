import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Customers} from "../../shared/model/customers";

@Component({
    moduleId: module.id,
    selector: 'customers-list',
    templateUrl: 'customers-list.component.html',
    styleUrls: ['customers-list.component.css']
})

export class CustomersListComponent {
    fieldCustomer:{name:string,value:string}[] = [
        {name: "имя", value: "name"},
        {name: "фамилия", value: "surname"},
        {name: "отчество", value: "patronymic"},
        {name: "сер. пасспорта", value: "passportSeries"},
        //{name: "номер. пасспорта", value: "passportId"},
        {name: "эл. почта", value: "email"},
        {name: "телефон", value: "phone"}
    ];

    @Input() customers:Customers[];
    @Output() delete:EventEmitter<Customers> = new EventEmitter();
    @Output() update:EventEmitter<Customers> = new EventEmitter();

    onUpdate(customer:Customers) {
        this.update.emit(customer);
    }

    onDelete(customer:Customers) {
        this.delete.emit(customer);
    }
}
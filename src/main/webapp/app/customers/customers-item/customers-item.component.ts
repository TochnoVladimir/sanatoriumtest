import {Component, Input, Output, EventEmitter } from '@angular/core';
import {Customers} from "../../shared/model/customers";

@Component({
    moduleId: module.id,
    selector: 'customers-item',
    templateUrl: 'customers-item.component.html',
    styleUrls: ['customers-item.component.css']
})

export class CustomersItemComponent {
    @Input() customer:Customers;
    @Output() delete = new EventEmitter();
    @Output() update = new EventEmitter();

    onUpdate() {
        this.update.emit(this.customer);
    }

    onDelete() {
        if(confirm("Вы хотите удалить запись?")){
            this.delete.emit(this.customer);
        }
    }
}
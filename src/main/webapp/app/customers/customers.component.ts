import {Component, OnInit} from '@angular/core';

import {Customers} from "../shared/model/customers";
import {CustomersService} from "../shared/service/customers.service";


@Component({
    moduleId: module.id,
    selector: 'customers',
    templateUrl: 'customers.component.html',
    styleUrls: ['customers.component.css']
})

export class CustomersComponent implements OnInit {
    customers:Customers[];
    customer:{customerIn:Customers, flaq:number};

    constructor(private customersService:CustomersService) {
        this.customers = [];
    }

    ngOnInit() {
        this.customersService.getCustomers().subscribe(customers => this.customers = customers);
    }

    create(customer:Customers) {
        this.customersService.createCustomers(customer).subscribe(
            customerResponse=> {
                if (!!customerResponse) {
                    if (!!customer.id) {
                        let index = this.findIndex(customerResponse);
                        this.customers[index] = customerResponse;
                    } else {
                        this.customers.push(customerResponse);
                    }

                }
            });
    }

    findIndex(item:Customers):number {
        for (var i = 0; i < this.customers.length; i++) {
            if (this.customers[i].id == item.id) {
                return i;
            }
        }
    }

    update(customer:Customers) {
        this.customer = {
            customerIn: customer,
            flaq: Math.random()
        }
    }

    delete(customer:Customers) {
        this.customersService.deleteCustomers(customer).subscribe(
            res => {
                if (res) {
                    let index = this.customers.indexOf(customer);
                    if (index > -1) {
                        this.customers.splice(index, 1);
                    }
                }
            });
    }
}
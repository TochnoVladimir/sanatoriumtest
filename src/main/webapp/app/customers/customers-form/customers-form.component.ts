import { Component, Output, Input, EventEmitter, OnChanges } from '@angular/core';
import {Customers} from "../../shared/model/customers";

@Component({
    moduleId: module.id,
    selector: 'customers-form',
    templateUrl: 'customers-form.component.html',
    styleUrls: ['customers-form.component.css']
})
export class CustomersFormComponent implements OnChanges {

    name:string = '';
    surname:string = '';
    patronymic:string = '';
    passportSeries:string = '';
    passportId:number;
    email:string = '';
    phone:string = '';

    @Input() customer:any;
    @Output() create:EventEmitter<Customers> = new EventEmitter();

//записать в шаблон инпутов html общее имя

    ngOnChanges() {
        if ((!!this.customer)) {
            this.name = this.customer.customerIn.name;
            this.surname = this.customer.customerIn.surname;
            this.patronymic = this.customer.customerIn.patronymic;
            this.passportSeries = this.customer.customerIn.passportSeries;
            this.passportId = this.customer.customerIn.passportId;
            this.phone = this.customer.customerIn.phone;
            this.email = this.customer.customerIn.email;
        }
    }

    onSubmit() {
        if (this.customer) {
            let customerUp:Customers = new Customers(this.name, this.surname, this.patronymic, this.passportSeries,
                this.passportId, this.email, this.phone);
            customerUp.id = this.customer.customerIn.id;
            this.create.emit(customerUp);
        }
        if (!this.customer) {
            let customer:Customers = new Customers(this.name, this.surname, this.patronymic, this.passportSeries,
                this.passportId, this.email, this.phone);
            this.create.emit(customer);
        }
        this.onReset();
    }

    onReset() {
        this.customer = undefined;
        this.name = '';
        this.surname = '';
        this.patronymic = '';
        this.passportSeries = '';
        this.passportId = null;
        this.phone = '';
        this.email = '';
    }
}
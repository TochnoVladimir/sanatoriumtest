package ru.artezio.dao;

import ru.artezio.model.Rooms;

import java.util.List;

public interface RoomsDao {

    List<Rooms> list();

    public Rooms create(Rooms rooms);

    public Rooms update(Rooms rooms);

    public Rooms findById(Long id);

    public void remove(Rooms rooms);

    public void remove(Long id);
}

package ru.artezio.dao;

import ru.artezio.model.Housing;

import java.util.List;

public interface HousingDao {

    List<Housing> list();

    public Housing create(Housing housing);

    public Housing update(Housing housing);

    public Housing findById(Long id);

    public void remove(Housing housing);

    public void remove(Long id);
}

package ru.artezio.dao;

import ru.artezio.model.UserRole;

import java.util.List;

public interface UserRoleDao {

    List<UserRole> list();

    public UserRole create(UserRole userRole);

    public UserRole update(UserRole userRole);

    public UserRole findById(Long id);

    public void remove(UserRole userRole);
}

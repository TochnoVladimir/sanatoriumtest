package ru.artezio.dao.impl;

import org.springframework.stereotype.Repository;
import ru.artezio.dao.CustomerDao;
import ru.artezio.model.Customer;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CustomerDaoImpl extends GenericDaoImpl<Customer, Long> implements CustomerDao {

    @Override
    public List<Customer> list() {
        return findObjectsByNamedQuery("findAllCustomers");
    }

    @Override
    public Customer create(Customer customer) {
        return save(customer);
    }

    @Override
    public Customer update(Customer customer) {
        return super.update(customer);
    }

    @Override
    public Customer findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void remove(Customer customer) {
        super.remove(customer);
    }

    @Override
    public void remove(Long id) {
        super.removeById(id);
    }
}

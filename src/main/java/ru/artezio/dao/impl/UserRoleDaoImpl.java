package ru.artezio.dao.impl;

import org.springframework.stereotype.Repository;
import ru.artezio.dao.UserRoleDao;
import ru.artezio.model.UserRole;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class UserRoleDaoImpl extends GenericDaoImpl<UserRole, Long> implements UserRoleDao {

    @Override
    public List<UserRole> list() {
        return findObjectsByNamedQuery("allUserRoles");
    }

    @Override
    public UserRole create(UserRole userRole) {
        return save(userRole);
    }

    @Override
    public UserRole update(UserRole userRole) {
        return super.update(userRole);
    }

    @Override
    public UserRole findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void remove(UserRole userRole) {
        super.remove(userRole);
    }
}

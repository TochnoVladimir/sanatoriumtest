package ru.artezio.dao.impl;

import org.springframework.stereotype.Repository;
import ru.artezio.dao.ConditionRoomDao;
import ru.artezio.model.ConditionRoom;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ConditionRoomDaoImpl extends GenericDaoImpl<ConditionRoom, Long> implements ConditionRoomDao {

    @Override
    public List<ConditionRoom> list() {
        return findObjectsByNamedQuery("findAllConditionRoom");
    }

    @Override
    public ConditionRoom create(ConditionRoom conditionRoom) {
        return save(conditionRoom);
    }

    @Override
    public ConditionRoom update(ConditionRoom conditionRoom) {
        return super.update(conditionRoom);
    }

    @Override
    public ConditionRoom findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void remove(ConditionRoom conditionRoom) {
        super.remove(conditionRoom);
    }

    @Override
    public void remove(Long id) {
        super.removeById(id);
    }
}

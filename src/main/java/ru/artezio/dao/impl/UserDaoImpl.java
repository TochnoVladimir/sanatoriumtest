package ru.artezio.dao.impl;

import org.springframework.stereotype.Repository;
import ru.artezio.accessory.QueryParam;
import ru.artezio.dao.UserDao;
import ru.artezio.model.User;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class UserDaoImpl extends GenericDaoImpl<User, Long> implements UserDao {

    @Override
    public List<User> list() {
        return findObjectsByNamedQuery("findAll");
    }

    @Override
    public User create(User user) {
        return save(user);
    }

    @Override
    public User update(User user) {
        return super.update(user);
    }

    @Override
    public User findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void remove(User user) {
        super.remove(user);
    }

    @Override
    public void remove(Long id) {
        super.removeById(id);
    }

    @Override
    public User findByLogin(String login) {
        List<QueryParam> params = new ArrayList<QueryParam>();
        params.add(new QueryParam("login", login));
        return findObjectByNamedQuery("findByLogin", params);
    }
}

package ru.artezio.dao.impl;

import org.springframework.stereotype.Repository;
import ru.artezio.dao.CategoryRoomDao;
import ru.artezio.model.CategoryRoom;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CategoryRoomDaoImpl extends GenericDaoImpl<CategoryRoom,Long> implements CategoryRoomDao {

    @Override
    public List<CategoryRoom> list() {
        return findObjectsByNamedQuery("findAllCategoryRoom");
    }

    @Override
    public CategoryRoom create(CategoryRoom categoryRoom) {
        return save(categoryRoom);
    }

    @Override
    public CategoryRoom update(CategoryRoom categoryRoom) {
        return super.update(categoryRoom);
    }

    @Override
    public CategoryRoom findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void remove(CategoryRoom categoryRoom) {
        super.remove(categoryRoom);
    }

    @Override
    public void remove(Long id) {
        super.removeById(id);
    }
}

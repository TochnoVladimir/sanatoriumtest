package ru.artezio.dao;

import ru.artezio.model.User;

import java.util.List;

public interface UserDao {

    List<User> list();

    public User create(User user);

    public User update(User user);

    public User findById(Long id);

    public void remove(User user);

    public void remove(Long id);

    public User findByLogin(String login);
}

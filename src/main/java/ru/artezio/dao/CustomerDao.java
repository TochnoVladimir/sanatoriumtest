package ru.artezio.dao;

import ru.artezio.model.Customer;

import java.util.List;

public interface CustomerDao {

    List<Customer> list();

    public Customer create(Customer customers);

    public Customer update(Customer customer);

    public Customer findById(Long id);

    public void remove(Customer customer);

    public void remove(Long id);
}

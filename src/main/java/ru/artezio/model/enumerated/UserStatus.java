package ru.artezio.model.enumerated;


public enum UserStatus {
    ACTIVE,
    BLOCKED
}

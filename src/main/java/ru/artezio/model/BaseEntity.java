package ru.artezio.model;

import javax.persistence.*;

@MappedSuperclass
public abstract class BaseEntity<U> {

    abstract public U getId();

    abstract public void setId(U id);
}

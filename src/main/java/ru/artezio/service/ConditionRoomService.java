package ru.artezio.service;

import ru.artezio.model.dto.ConditionRoomDto;

import java.util.List;

public interface ConditionRoomService {

    List<ConditionRoomDto> list();

    public ConditionRoomDto create(ConditionRoomDto conditionRoom);

    public ConditionRoomDto findById(Long id);

    public void remove(Long id);
}

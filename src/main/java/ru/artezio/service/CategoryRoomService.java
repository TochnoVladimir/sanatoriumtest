package ru.artezio.service;

import ru.artezio.model.dto.CategoryRoomDto;

import java.util.List;

public interface CategoryRoomService {

    List<CategoryRoomDto> list();

    public CategoryRoomDto create(CategoryRoomDto categoryRoom);

    public CategoryRoomDto findById(Long id);

    public void remove(Long id);
}

package ru.artezio.service;

import ru.artezio.model.dto.HousingDto;

import java.util.List;

public interface HousingService {

    List<HousingDto> list();

    public HousingDto create(HousingDto housing);

    public HousingDto findById(Long id);

    public void remove(Long id);
}

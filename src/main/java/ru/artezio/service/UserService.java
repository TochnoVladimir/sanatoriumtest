package ru.artezio.service;

import ru.artezio.model.User;
import ru.artezio.model.UserRole;
import ru.artezio.model.dto.UserDto;

import java.util.List;

public interface UserService {

    List<UserDto> list();

    public UserDto create(UserDto user);

    public UserDto findById(Long id);

    public void remove(Long id);

    public UserDto findByLogin(String login);

    public List<UserRole> listUserRole();
}

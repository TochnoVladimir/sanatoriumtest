package ru.artezio.service;

import ru.artezio.model.dto.RoomsDto;

import java.util.List;

public interface RoomsService {

    List<RoomsDto> list();

    public RoomsDto create(RoomsDto rooms);

    public RoomsDto findById(Long id);

    public void remove(Long id);
}

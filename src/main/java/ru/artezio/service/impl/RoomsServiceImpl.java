package ru.artezio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.artezio.dao.RoomsDao;
import ru.artezio.model.Rooms;
import ru.artezio.model.dto.RoomsDto;
import ru.artezio.service.RoomsService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RoomsServiceImpl extends BaseService implements RoomsService {

    @Autowired RoomsDao roomsDao;

    @Override
    public List<RoomsDto> list() {
        return mappingList(roomsDao.list(), RoomsDto.class);
    }

    @Override
    public RoomsDto create(RoomsDto rooms) {
        return mappingObject(roomsDao.create(mappingObject(rooms, Rooms.class)), RoomsDto.class);
    }

    @Override
    public RoomsDto findById(Long id) {
        return mappingObject(roomsDao.findById(id), RoomsDto.class);
    }

    @Override
    public void remove(Long id) {
        roomsDao.remove(id);
    }
}

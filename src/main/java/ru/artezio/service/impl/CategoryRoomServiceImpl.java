package ru.artezio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.artezio.dao.CategoryRoomDao;
import ru.artezio.model.CategoryRoom;
import ru.artezio.model.dto.CategoryRoomDto;
import ru.artezio.service.CategoryRoomService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CategoryRoomServiceImpl extends BaseService implements CategoryRoomService {

    @Autowired CategoryRoomDao categoryRoomDao;

    @Override
    public List<CategoryRoomDto> list() {
        return mappingList(categoryRoomDao.list(), CategoryRoomDto.class);
    }

    @Override
    public CategoryRoomDto create(CategoryRoomDto categoryRoomDto) {
        return mappingObject(categoryRoomDao.create(mappingObject(categoryRoomDto, CategoryRoom.class)), CategoryRoomDto.class);
    }

    @Override
    public CategoryRoomDto findById(Long id) {
        return mappingObject(categoryRoomDao.findById(id), CategoryRoomDto.class);
    }

    @Override
    public void remove(Long id) {
        categoryRoomDao.remove(id);
    }
}

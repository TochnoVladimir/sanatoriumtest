package ru.artezio.service.impl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class BaseService {

    @Autowired private Mapper mapper;

    public <T, U> U mappingObject(T source, Class<U> destinationClass) {
        return mapper.map(source, destinationClass);
    }

    public <T extends List, U> List<U> mappingList(T source, Class<U> destinationClass) {
        List<U> mapList = new ArrayList<U>(source.size());
        for (Object mapObject : source) {
            mapList.add(mapper.map(mapObject, destinationClass));
        }
        return mapList;
    }
}

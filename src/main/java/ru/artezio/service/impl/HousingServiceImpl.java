package ru.artezio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.artezio.dao.HousingDao;
import ru.artezio.model.Housing;
import ru.artezio.model.dto.HousingDto;
import ru.artezio.service.HousingService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class HousingServiceImpl extends BaseService implements HousingService {

    @Autowired HousingDao housingDao;

    @Override
    public List<HousingDto> list() {
        return mappingList(housingDao.list(), HousingDto.class);
    }

    @Override
    public HousingDto create(HousingDto housing) {
        return mappingObject(housingDao.create(mappingObject(housing, Housing.class)), HousingDto.class);
    }

    @Override
    public HousingDto findById(Long id) {
        return mappingObject(housingDao.findById(id), HousingDto.class);
    }

    @Override
    public void remove(Long id) {
        housingDao.remove(id);
    }
}

package ru.artezio.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.artezio.model.User;
import ru.artezio.model.UserRole;
import ru.artezio.model.dto.UserDto;
import ru.artezio.service.UserService;

import java.util.*;

@Service("provider")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserDto user = userService.findByLogin(login);
        if (user != null) {
            List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
            Iterator<UserRole> iterator = user.getRoles().iterator();
            while (iterator.hasNext()) {
                roles.add(new SimpleGrantedAuthority("ROLE_" + iterator.next().getName()));
            }
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), roles);
            return userDetails;
        }
        return null;
    }
}

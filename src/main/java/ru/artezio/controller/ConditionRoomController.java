package ru.artezio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.accessory.response.BaseResponse;
import ru.artezio.exceptions.ValidateException;
import ru.artezio.model.dto.ConditionRoomDto;
import ru.artezio.service.ConditionRoomService;

@RestController
@RequestMapping(value = "/conditions")
public class ConditionRoomController extends BaseController {

    @Autowired ConditionRoomService conditionRoomService;

    @RequestMapping
    public ResponseEntity<BaseResponse> listConditionRooms() {
        try {
            return createSuccessResponse(conditionRoomService.list());
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<BaseResponse> createConditionRoom(@RequestBody ConditionRoomDto conditionRoom) {
        try {
            validationObject(conditionRoom);
            return createSuccessResponse(conditionRoomService.create(conditionRoom));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<BaseResponse> updateConditionRoom(@PathVariable Long id, @RequestBody ConditionRoomDto conditionRoom) {
        try {
            validationObject(conditionRoom);
            conditionRoom.setId(id);
            return createSuccessResponse(conditionRoomService.create(conditionRoom));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<BaseResponse> findConditionRoomById(@PathVariable Long id) {
        try {
            return createSuccessResponse(conditionRoomService.findById(id));
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BaseResponse> deleteConditionRoom(@PathVariable Long id) {
        try {
            conditionRoomService.remove(id);
            return createVoidResponse();
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }
}

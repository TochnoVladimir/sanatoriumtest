package ru.artezio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.accessory.response.BaseResponse;
import ru.artezio.exceptions.ValidateException;
import ru.artezio.model.dto.HousingDto;
import ru.artezio.service.HousingService;

@RestController
@RequestMapping(value = "/housings")
public class HousingController extends BaseController {

    @Autowired HousingService housingService;

    @RequestMapping
    public ResponseEntity<BaseResponse> listHousings() {
        try {
            return createSuccessResponse(housingService.list());
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<BaseResponse> createHousing(@RequestBody HousingDto housing) {
        try {
            validationObject(housing);
            return createSuccessResponse(housingService.create(housing));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<BaseResponse> updateHousing(@PathVariable Long id, @RequestBody HousingDto housing) {
        try {
            validationObject(housing);
            housing.setId(id);
            return createSuccessResponse(housingService.create(housing));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<BaseResponse> findHousingById(@PathVariable Long id) {
        try {
            return createSuccessResponse(housingService.findById(id));
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BaseResponse> deleteHousing(@PathVariable Long id) {
        try {
            housingService.remove(id);
            return createVoidResponse();
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }
}

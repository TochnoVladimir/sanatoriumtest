package ru.artezio.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.artezio.accessory.response.BaseResponse;
import ru.artezio.accessory.response.FailedResponse;
import ru.artezio.accessory.response.SuccessResponse;
import ru.artezio.exceptions.ApiException;
import ru.artezio.exceptions.InternalException;
import ru.artezio.exceptions.ValidateException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;

public abstract class BaseController {

    public ResponseEntity<BaseResponse> createSuccessResponse(Object object) {
        return new ResponseEntity<BaseResponse>(new SuccessResponse(object), HttpStatus.OK);
    }

    public ResponseEntity<BaseResponse> createVoidResponse() {
        return new ResponseEntity<BaseResponse>(new BaseResponse(true), HttpStatus.OK);
    }

    public ResponseEntity<BaseResponse> createInternalExceptionResponse() {
        return new ResponseEntity<BaseResponse>(new FailedResponse(new InternalException().getMessage()), HttpStatus.OK);
    }

    public ResponseEntity<BaseResponse> createFailedResponse(ApiException exception) {
        ResponseEntity<BaseResponse> responseEntity;
        if (exception instanceof ValidateException) {
            responseEntity = new ResponseEntity<BaseResponse>(new FailedResponse(
                    exception.getMessage()), HttpStatus.OK);
            return responseEntity;
        }
        responseEntity = new ResponseEntity<BaseResponse>(new FailedResponse(exception.getMessage()), HttpStatus.OK);
        return responseEntity;
    }

    public void validationObject(Object object) throws ValidateException {
        Map<String, String> errorField = new HashMap<String, String>();
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();

        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object);
        for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
            errorField.put(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
        }
        if (!errorField.isEmpty())
            throw new ValidateException(errorField);
    }
}

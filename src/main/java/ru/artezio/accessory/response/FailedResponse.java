package ru.artezio.accessory.response;

public class FailedResponse extends BaseResponse {

    private final String errorMessage;

    public FailedResponse(String errorMessage) {
        super(false);
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}

package ru.artezio.accessory.response;

public class BaseResponse {

    private final Boolean success;

    public BaseResponse(Boolean success) {
        this.success = success;
    }

    public Boolean getSuccess() {
        return success;
    }
}

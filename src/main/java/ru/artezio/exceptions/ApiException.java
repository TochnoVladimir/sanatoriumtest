package ru.artezio.exceptions;

public abstract class ApiException extends Exception {

    private String message;
    private String code;

    @Override
    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
